# ClipCap

Frecuentemente necesito una manera de ir capturando retazos de información, casi siempre direcciones web para descargas posteriores, y lo ideal para mí es, en estos casos, almacenar esas direcciones en un archivo de texto que le puedo pasar después a [wget][1] o a la mayoría de los [administradores de descargas][2].

Para la operación esencial de captura de datos, hay una buena cantidad de [administradores de portapapeles][3] para GNU/Linux, varios de ellos muy buenos (mi favorito es [Diodon][4]) y probablemente más de uno realiza la captura en un archivo de texto oculto en algún lado, pero por algún motivo, en vez de ponerme a investigar sobre este asunto, decidí escribir mi propio programa en Python que hiciera exactamente lo que necesito.

**ClipCap** es un script en Python que se ejecuta en modo consola y va almacenando en un archivo de texto el contenido que se copia al portapapeles. El nombre de archivo predeterminado es "clipcap.txt" y se genera en la ubicación desde donde se ejecuta el programa.

Como es habitual, para saber más acerca de la sintaxis de ClipCap, basta con ejecutarlo con el parámetro **-h**

    usage: clipcap.py [-h] [-s] [-a] [nombre]
    
    Captura datos en portapapeles.
    
    positional arguments:
      nombre        Nombre de archivo [clipcap.txt]
    
    optional arguments:
      -h, --help    show this help message and exit
      -s, --strip   Recorta caracteres innecesarios.
      -a, --append  Agrega datos a archivo existente.
    

ClipCap requiere la presencia de [Pyperclip][5], un módulo multiplataforma que permite acceder al portapapeles desde Python. Puede instalarse con nuestro administrador de módulos favorito, o descargarse directamente:

    sudo pip install pyperclip || wget http://coffeeghost.net/src/pyperclip.py
    

Pyperclip, por su parte, requiere que tengamos instalado en nuestro sistema las utilidades [xclip][6] o [xsel][7], o en su defecto los módulos **gtk** o **PyQt4**.

De manera predeterminada, el archivo será inicializado cada vez que se ejecute ClipCap. Para evitar que se pierdan los datos capturados en una sesión anterior, se debe agregar el parámetro **-a** a la línea de comandos.

ClipCap puede descargarse desde su [repositorio][8] [aquí][9].

 [1]: https://www.gnu.org/software/wget/
 [2]: http://www.linuxb.in/2012/01/best-8-download-managersaccelerators.html
 [3]: http://www.webupd8.org/2010/08/best-linux-clipboard-manager.html
 [4]: http://www.webupd8.org/2011/01/diodon-lightweight-clipboard-manager.html
 [5]: http://coffeeghost.net/2010/10/09/pyperclip-a-cross-platform-clipboard-module-for-python/
 [6]: http://sourceforge.net/projects/xclip/
 [7]: http://www.vergenet.net/~conrad/software/xsel/
 [8]: https://bitbucket.org/czayas/clipcap
 [9]: https://bitbucket.org/czayas/clipcap/get/master.zip