#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
ClipCap - Versión 1.1 (18/02/2014) Python 2.7.5+
Copyleft 18/02/2014 Carlos Zayas Guggiari <carlos@zayas.org>
'''

# -----------------------------------------------------------------------------
# Importación de módulos
# -----------------------------------------------------------------------------

import termios, fcntl, sys, os, time, argparse, struct
sys.path.append(os.path.abspath("SO_site-packages"))
# sudo pip install pyperclip || wget http://coffeeghost.net/src/pyperclip.py
import pyperclip

# -----------------------------------------------------------------------------
# Configurar los parámetros de entrada
# -----------------------------------------------------------------------------

parser = argparse.ArgumentParser(description="Captura datos en portapapeles.")
parser.add_argument("nombre",
                    nargs="?",
                    action="store",
                    default="clipcap.txt",
                    help="Nombre de archivo [clipcap.txt]")
parser.add_argument("-s", "--strip",
                    help="Recorta caracteres innecesarios.",
                    action="store_true")
parser.add_argument("-a", "--append",
                    help="Agrega datos a archivo existente.",
                    action="store_true")
args = parser.parse_args()

# -----------------------------------------------------------------------------
# Funciones
# -----------------------------------------------------------------------------

def imprimir(cadena):
    sys.stdout.write(cadena)
    sys.stdout.flush()

# -----------------------------------------------------------------------------
# Bloque principal
# -----------------------------------------------------------------------------

if sys.stdout.isatty():
    h, w = struct.unpack('hh',fcntl.ioctl(sys.stdout,termios.TIOCGWINSZ,'1234'))
else:
    h, w = 0,0 # REVISAR - OJO

os.system("clear")

modo = "a" if args.append else "w"

archivo = open(args.nombre, modo)

viejo = pyperclip.paste()

imprimir("Copie 'q' o presione Control+C para terminar.\n")

while viejo.lower() <> "q":
    try:
        nuevo = pyperclip.paste()
        if viejo != nuevo:
            viejo = nuevo
            if viejo.lower() <> "q":
                clip = nuevo.strip() if args.strip else nuevo
                imprimir(str(nuevo)[:w] + "\n")
                archivo.write(nuevo + "\n")
        time.sleep(0.1)
    except KeyboardInterrupt:
        viejo = "q"

archivo.close()
